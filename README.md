# README #

Exemple de design pattern SOLID

### What is this repository for? ###

* Design pattern de responsabilité des classes.
* Calcul du prix d'un panier:
 - Prix d'une boite = 5 francs
 - Les boites peuvent contenir des boites qui elles-mêmes peuvent contenir des boites ...
 
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
