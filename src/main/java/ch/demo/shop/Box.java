package ch.demo.shop;

/**
 * @author Aliou Ka on 11/21/16.
 */
public class Box extends Container implements Article {

	public static final int DEFAULT_BOX_PRICE = 5;

	public int getPrice() {
		return DEFAULT_BOX_PRICE + articles.stream().mapToInt(Article::getPrice).sum();
	}

	public static Box newBox() {
		return new Box();
	}
}
