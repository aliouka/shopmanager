package ch.demo.shop;

/**
 * @author Aliou Ka on 11/21/16.
 */
public interface Article {

	int getPrice();
}
