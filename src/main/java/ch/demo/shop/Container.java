package ch.demo.shop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Aliou Ka on 11/21/16.
 */
public abstract class Container {

	List<Article> articles = new ArrayList<>();

	Container add(Article... articles) {
		Collections.addAll(this.articles, articles);
		return this;
	}
}
