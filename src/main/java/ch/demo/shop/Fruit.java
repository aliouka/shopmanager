package ch.demo.shop;

/**
 * @author Aliou Ka on 11/21/16.
 */
public class Fruit implements Article {

	private String name;
	private int price;

	public Fruit(String name, int price) {
		this.name = name;
		this.price = price;
	}

	public static Fruit newFruit(String name, int price) {
		return new Fruit(name, price);
	}

	public int getPrice() {
		return price;
	}

}
