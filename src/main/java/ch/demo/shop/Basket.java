package ch.demo.shop;

/**
 * @author Aliou Ka on 11/21/16.
 */
public class Basket extends Container {

	public int getPrice() {
		return articles.stream().mapToInt(Article::getPrice).sum();
	}

	public static Basket newBasket() {
		return new Basket();
	}
}
