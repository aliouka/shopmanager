package ch.demo.shop;

/**
 * @author Aliou Ka on 11/21/16.
 */
public class Main {

	public static void main(String[] args) {

		Basket basket = Basket.newBasket();
		Box articleBox = Box.newBox(), //
				emptyBox = Box.newBox();

		Fruit banane = Fruit.newFruit("banane", 2), //
				orange = Fruit.newFruit("orange", 3), //
				ananas = Fruit.newFruit("ananas", 13);

		articleBox.add(ananas, orange);
		basket.add(banane).add(articleBox, emptyBox);

		System.out.println(String.format("Prix du panier: %d franc(s)", basket.getPrice()));

	}

}
